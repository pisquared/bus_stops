from pprint import pprint
from math import sqrt

line_1 = {
    'name': '1_east',
    'stops': [
        [0, 2],
        [1, 2],
        [2, 2],
        [3, 2],
    ]
}

line_2 = {
    'name': '2_south',
    'stops': [
        [2, 3],
        [2, 2],
        [2, 1],
        [2, 0],
    ]
}

REGISTERED_LINES = [
    line_1,
    line_2,
]

LINES = {line['name']: line['stops'] for line in REGISTERED_LINES}

def distance_to_stop(loc, stop):
    diff_x = loc[0] - stop[0]
    diff_y = loc[1] - stop[1]
    return abs(sqrt(pow(diff_x, 2) + pow(diff_y, 2)))


def order_stops(loc, lines):
    ordered_stops = []
    for line_name, stops in lines.iteritems():
        for stop in stops:
            distance = distance_to_stop(loc, stop)
            stop_meta = {
                'line': line_name,
                'stop': stop,
                'distance': distance
            }
            ordered_stops.append(stop_meta)
    return sorted(ordered_stops, key=lambda x: x['distance'])


def find_path(loc_A, loc_B):
    path = []

    closest_stops_to_A = order_stops(loc_A, LINES)
    closest_stops_to_B = order_stops(loc_B, LINES)

    path.append({'walk': closest_stops_to_A[0]['distance']})

    line_A = closest_stops_to_A[0]['line']
    line_B = closest_stops_to_B[0]['line']

    stops = {'line': line_A, 'stops': []}
    if line_A == line_B:
        line_stops = LINES[line_A]
        stop_A = closest_stops_to_A[0]['stop']
        stop_B = closest_stops_to_B[0]['stop']
        start_appending = False
        for stop in line_stops:
            if stop == stop_A:
                start_appending = True
            if start_appending:
                stops['stops'].append(stop)
            if stop_B == stop:
                break
        path.append(stops)
        path.append({'walk': closest_stops_to_B[0]['distance']})
    else:
        line_1_name = line_A
        line_stops = LINES[line_A]
        start_appending = False
        temp_stops = []
        for stop in line_stops:
            if stop == stop_A:
                start_appending = True
            if start_appending:
                temp_stops['stops'].append(stop)
        for line_2_name, line_2_stops in LINES.iteritems():
            if line_1_name == line_2_name:
                continue
            for stop in temp_stops:
                ordered_stops = order_stops(stop, line_2_stops)
            # TODO
    return path

path_A_B = find_path([1, 2], [3, 2])

assert path_A_B == [
    {'walk': 0.0},
    {'line': '1_east', 'stops': [[1, 2], [2, 2], [3, 2]]},
    {'walk': 0.0}
]

# TESTS
LOC_A = [0, 2]
LOC_B = [2, 0]

assert distance_to_stop([0, 0], [0, 0]) == 0
assert distance_to_stop([0, 0], [2, 2]) == sqrt(8)
assert distance_to_stop([2, 2], [0, 0]) == sqrt(8)

ordered_stops_A = order_stops(LOC_A, LINES)
ordered_stops_B = order_stops(LOC_B, LINES)

assert ordered_stops_A == [
    {'distance': 0.0, 'line': '1_east', 'stop': [0, 2]},
    {'distance': 1.0, 'line': '1_east', 'stop': [1, 2]},
    {'distance': 2.0, 'line': '2_south', 'stop': [2, 2]},
    {'distance': 2.0, 'line': '1_east', 'stop': [2, 2]},
    {'distance': 2.23606797749979, 'line': '2_south', 'stop': [2, 3]},
    {'distance': 2.23606797749979, 'line': '2_south', 'stop': [2, 1]},
    {'distance': 2.8284271247461903, 'line': '2_south', 'stop': [2, 0]},
    {'distance': 3.0, 'line': '1_east', 'stop': [3, 2]}
]

assert ordered_stops_B == [
    {'distance': 0.0, 'line': '2_south', 'stop': [2, 0]},
    {'distance': 1.0, 'line': '2_south', 'stop': [2, 1]},
    {'distance': 2.0, 'line': '2_south', 'stop': [2, 2]},
    {'distance': 2.0, 'line': '1_east', 'stop': [2, 2]},
    {'distance': 2.23606797749979, 'line': '1_east', 'stop': [1, 2]},
    {'distance': 2.23606797749979, 'line': '1_east', 'stop': [3, 2]},
    {'distance': 2.8284271247461903, 'line': '1_east', 'stop': [0, 2]},
    {'distance': 3.0, 'line': '2_south', 'stop': [2, 3]}
]

path_A_B = find_path(LOC_A, LOC_B)
assert path_A_B == [
    {'walk': 0},
    {'line': '1_east', 'stops': [[0, 2], [1, 2], [2, 2]]},
    {'walk': 0},
    {'line': '2_south', 'stops': [[2, 2], [2, 1], [2, 0]]},
    {'walk': 0},
]
